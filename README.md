# yii-swageer-element

#### 介绍
整合框框 Yii2 + swagger + vue-element-admin + yii2-admin实现通用后台

#### 软件架构
软件架构说明


#### 安装教程【PHP】
修改了部分yii2-admin源代码，所以本项目上传了 ./vendor目录
1.  git clone https://gitee.com/wslys/yii-swageer-element.git
2.  创建数据swagger_element, 导入 docs/install.sql 数据
3.  nginx配置
```
server {
	listen       80;
	server_name  loc.swagger.element.com;
	root   "C:\_development\code\swagger-element\api\web";
	index index.php;
	location / {
		try_files $uri $uri/ /index.php$is_args$args;
		#autoindex  on;
	}
	location ~ \.php(.*)$ {
		fastcgi_pass   127.0.0.1:9000;
		fastcgi_index  index.php;
		fastcgi_split_path_info  ^((?U).+\.php)(/?.+)$;
		fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
		fastcgi_param  PATH_INFO  $fastcgi_path_info;
		fastcgi_param  PATH_TRANSLATED  $document_root$fastcgi_path_info;
		include        fastcgi_params;
	}
}
```
4. windows 修改hosts文件，C:\Windows\System32\drivers\etc\hosts  
添加如下代码
```
127.0.0.1 loc.swagger.element.com
```
5. 浏览器访问 loc.swagger.element.com
5. 权限管理 loc.swagger.element.com/admin
5. API文档 loc.swagger.element.com/site/doc

#### 安装教程【客户端】
```
1. cd client
2. npm install
3. npm run dev
4. 浏览器访问 http://localhost:9527/
```

#### 效果图
![输入图片说明](https://gitee.com/wslys/yii-swageer-element/raw/master/docs/img/1.png "在这里输入图片标题")  

![输入图片说明](https://gitee.com/wslys/yii-swageer-element/raw/master/docs/img/1-1.png "在这里输入图片标题")

![输入图片说明](https://gitee.com/wslys/yii-swageer-element/raw/master/docs/img/2.png "在这里输入图片标题")


#### 使用说明

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
