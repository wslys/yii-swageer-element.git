<?php
return [
    'adminEmail' => 'admin@example.com',
    'AUTH_ACCESS_TOKEN_EXPIRER_TIME' => 60 * 60
];
