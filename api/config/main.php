<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'language' => 'zh-CN',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'aliases' => [
        '@mdm/admin' => '@vendor/mdmsoft/yii2-admin',
    ],
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
            // 'layout' => '@vendor/mdmsoft/yii2-admin/views/layouts/left-menu' // 【在yii-admin内部使用布局页面使用】
        ],
        'v1' => [
            'class' => 'api\modules\v1\Module',
        ]
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            '*',
            'site/*',
            'admin/*',
            'v1/user/login'
        ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-api',
        ],
        'authManager'=> [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'user' => [
            'identityClass' => 'api\modules\models\AdminUser',
            'enableAutoLogin' => true,
        ],
        'session' => [
            // this is the name of the session cookie used for login on the api
            'name' => 'advanced-api',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'GET v1/articles' => 'v1/article/index',
                'POST v1/article' => 'v1/article/create',
                'PUT v1/article/<id:\d+>' => 'v1/article/update',
                'GET v1/article/<id:\d+>' => 'v1/article/view',
                'DELETE v1/article/<id:\d+>' => 'v1/article/delete'
            ],
        ],

    ],
    'params' => $params,
];
