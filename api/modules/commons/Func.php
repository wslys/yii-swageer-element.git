<?php
namespace api\modules\commons;

use Yii;

/**
 * 全局工具函数
 *
 * Class Func
 * @package api\modules\commons
 */
class Func
{
    /**
     * 把返回的数据集转换成Tree
     *
     * @param array $list 要转换的数据集
     * @param string $pk  创建基于主键的数组引用
     * @param string $pid parent标记字段
     * @param string $child evel标记字段
     * @param int $root 根
     * @return array
     */
    public static function ArrayToTree($list, $pk='id', $pid = 'pid', $child = 'childs', $root = 0) {
        if (is_array($list) == false) return [];

        // 创建基于主键的数组引用
        $aRefer = array();
        foreach ($list as $key => $data) {
            $aRefer[$data[$pk]] = &$list[$key];
        }

        $tree = [];
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[$pid];
            if ($root == $parentId) {
                $tree[] = &$list[$key];
            } else {
                if (isset($aRefer[$parentId])) {
                    $parent = &$aRefer[$parentId];
                    $parent[$child][] = &$list[$key];
                }
            }
        }

        return $tree;
    }

    /**
     * 把返回的数据集转换成Tree
     * @param array $rows 要转换的数据集
     * @param string $id 主键id
     * @param string $pid parent标记字段
     * @param string $child 子节点存放的位置
     * @return array
     */
    public static function ArrayToTree2($rows, $id='id', $pid='pid', $child = 'child') {
        $items = array();

        foreach ($rows as $row) {
            $items[$row[$id]] = $row;
        }

        foreach ($items as $item) {
            $items[$item[$pid]][$child][$item[$id]] = &$items[$item[$id]];
        }

        return isset($items[0][$child]) ? $items[0][$child] : [];
    }

    /**
     * 把二维数组转化为树状结构数组
     *
     * [@param array  $arr
     * @param  int    $pid
     * @param  string $idName    id的名称,默认是id
     * @param  string $pidName   pid的名称,默认是pid
     * @param  string $childName 下级的key名称，默认是child
     * @return array  $tree      生成的树状数组
     * @since] 2016-12-05
     * [@author] mingazi@163.com
     * [
     */
    public static function createTree($arr = array(), $pid = 0, $idName = 'id', $pidName = 'pid', $childName = 'children')
    {
        $tree = array();
        foreach($arr as $k => $v)
        {
            if($v[$pidName] == $pid)
            {
                $tmp = $arr[$k];
                unset($arr[$k]);
                $tmp[$childName] = self::createTree($arr, $v[$idName], $idName, $pidName, $childName);
                $tree[] = $tmp;
            }
        }
        return $tree;
    }

    /**
     * 调试日志
     * @param string $msg
     * @param string $file
     */
    public static function Pre($msg='', $file='') {
        file_put_contents($file ? $file:'file.json', json_encode($msg, JSON_UNESCAPED_UNICODE) . "\r\n", FILE_APPEND);
    }

    // 格式化菜单
    public static function formatMenuData($menu) {
        $data = json_decode($menu['data'], true);
        $items = $menu['children'];
        /*$data['label'] = $menu['name'];
        $data['url'] = isset($menu['url'])?$menu['url']:'';*/
        $return = $data;

        // 没配置图标的显示默认图标
        // (!isset($return['icon']) || !$return['icon']) && $return['icon'] = 'fa fa-circle-o';

        $items && $return['children'] = $items;

        return $return;
    }

    // Log
    public static function log($msg=[], $file='') {
        file_put_contents($file?:Yii::getAlias("@api").'/runtime/logs/debug-log.log', json_encode($msg, JSON_UNESCAPED_UNICODE) . "\r\n", FILE_APPEND);
    }
}
