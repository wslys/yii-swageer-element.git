<?php


namespace api\modules\v1\rules\article;


use api\modules\v1\models\Article;
use common\models\User;
use Yii;
use api\modules\v1\rules\BaseRule;
use yii\db\Exception;

/**
 * 文章查看规则类
 *
 * Class ArticleViewRule
 *
 * @package api\modules\v1\rules\article
 */
class ArticleViewRule extends BaseRule
{
    // 规则名称【建议与规则类名一致，这样方便查找】
    public $name = 'articleViewRule';

    public function execute($user, $item, $params)
    {
        if (!$this->loginUser || !($this->loginUser instanceof User)) {
            return false;
        }

        $model = Article::findOne(['id'=>$user, 'uid'=>$this->loginUser->id]);
        if ($model) {
            return true;
        }
        return false;
    }
}
