<?php


namespace api\modules\v1\rules;

use Yii;
use yii\rbac\Rule;

/**
 *
 * @property-read null $loginUser
 */
abstract class BaseRule extends Rule
{
    // 系统登陆用户
    public function getLoginUser() {
        if (!Yii::$app->user->isGuest) {
            return Yii::$app->user->identity;
        }
        return null;
    }
}
