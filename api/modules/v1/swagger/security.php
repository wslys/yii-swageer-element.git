<?php
namespace api\modules\v1\swagger;

/**
 * @OA\Info(
 *     version="1.0",
 *     title="Yii2 + Swagger + vue-element-admin 项目后台接口文档",
 *     termsOfService= "http://example.com/terms/",
 *     @OA\Contact(
 *         name="API支持",
 *         url="http://loc.swagger.element.com/",
 *         email="wsl_ys@163.com"
 *     ),
 *     @OA\License(
 *         name="Nginx 1.18",
 *         url="http://nginx.org/"
 *     )
 * ),
 * @OA\SecurityScheme(
 *     securityScheme="bearerAuth",
 *     in="header",
 *     name="Authorization",
 *     type="http",
 *     scheme="Bearer",
 *     bearerFormat="JWT",
 * ),
 */

