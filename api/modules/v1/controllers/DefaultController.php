<?php

namespace api\modules\v1\controllers;


use api\modules\v1\base\RestApiBaseController;

/**
 * Default controller for the `v1` module
 */
class DefaultController extends RestApiBaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
