<?php

namespace api\modules\v1\controllers;


use api\modules\models\LoginForm;
use api\modules\v1\base\RestApiBaseController;
use api\modules\commons\Func;
use api\modules\v1\models\Article;
use api\modules\v1\models\search\ArticleSearch;
use mdm\admin\components\MenuHelper;
use mdm\admin\models\Menu as MenuModel;
use mdm\admin\models\Route;
use phpDocumentor\Reflection\Types\This;
use Yii;
use yii\db\StaleObjectException;

/**
 * Class ArticleController
 *
 * @OA\Tag(
 *     name="文章管理",
 *     description="文章管理接口文档",
 *     *
 * )
 * @package api\modules\v1\controllers
 */
class ArticleController extends RestApiBaseController
{
    /**
     * * @OA\Get (
     *     path="/v1/articles",
     *     tags={"文章管理"},
     *     summary="文章列表",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="页码",
     *         @OA\Schema(
     *             type="string",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="page_size",
     *         in="query",
     *         description="每页条目数",
     *         @OA\Schema(
     *             type="int",
     *             example=10
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid ID supplied"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Pet not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Validation exception"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation"
     *     )
     * )
     * @param int $page
     * @param int $page_size
     * @return array
     */
    public function actionIndex($page=1, $page_size=10)
    {
        $searchModel = new ArticleSearch();
        $params = $this->request->queryParams;
        $dataProvider = $searchModel->search($params, $page_size);

        return $this->success([
            'page'=> [
                'count'=>$dataProvider->totalCount,
                'page' => $page,
                'pageSize' => $page_size,
            ],
            'list' => $dataProvider->getModels()
        ]);
    }

    /**
     * @OA\Post(
     *     path="/v1/article",
     *     tags={"文章管理"},
     *     summary="创建文章",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="title",
     *                     type="string",
     *                     description="文章标题"
     *                 ),
     *                 @OA\Property(
     *                     property="type",
     *                     type="int",
     *                     description="文章类型"
     *                 ),
     *                 @OA\Property(
     *                     property="connect",
     *                     type="string",
     *                     description="文章内容"
     *                 ),
     *                 example={"title": "文章标题", "type": 10, "connect": "文章内容"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionCreate() {
        $model = new Article();
        if ($model->load(['Article'=>$this->PostData()]) && $model->save()) {
            return $this->success($model);
        }
        return $this->fail('文章创建失败', $model->getErrors());
    }

    /**
     * 更新文章
     *
     * @OA\Put (
     *     path="/v1/article/{id}",
     *     tags={"文章管理"},
     *     summary="更新文章",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="文章ID",
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="title",
     *                     type="string",
     *                     description="文章标题"
     *                 ),
     *                 @OA\Property(
     *                     property="type",
     *                     type="int",
     *                     description="文章类型"
     *                 ),
     *                 @OA\Property(
     *                     property="connect",
     *                     type="string",
     *                     description="文章内容"
     *                 ),
     *                 example={"title": "文章标题", "type": 10, "connect": "文章内容"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     * @param $id
     * @return array
     */
    public function actionUpdate($id) {
        $model = Article::findOne($id);
        if (!$model) {
            return $this->fail('无效文章ID', ['id'=>$id]);
        }
        if ($model->load(['Article'=>$this->PutData()]) && $model->save()) {
            return $this->success($model, '', $this->PostData());
        }
        return $this->fail('文章更新失败', $model->getErrors());
    }

    /**
     * 文章详情
     *
     * @OA\Get (
     *     path="/v1/article/{id}",
     *     tags={"文章管理"},
     *     summary="文章详情",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="文章ID",
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     * @param $id
     * @return array
     */
    public function actionView($id) {
        $model = Article::findOne($id);
        if (!$model) {
            return $this->fail('无效文章ID', ['id'=>$id]);
        }
        return $this->success($model);
    }

    /**
     * 删除文章
     *
     * @OA\Delete (
     *     path="/v1/article/{id}",
     *     tags={"文章管理"},
     *     summary="删除文章",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="文章ID",
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     * @param $id
     * @return array
     */
    public function actionDelete($id) {
        $model = Article::findOne($id);
        if (!$model) {
            return $this->fail('无效文章ID', ['id'=>$id]);
        }

        try {
            if ($model->delete()) {
                return $this->success();
            }
        } catch (StaleObjectException $e) {
            return $this->fail('删除失败', $e->getMessage());
        } catch (\Throwable $e) {
            return $this->fail('删除失败', $e->getMessage());
        }
    }
}
