<?php

namespace api\modules\v1\controllers;


use api\modules\v1\base\RestApiBaseController;
use api\modules\commons\Func;
use api\modules\models\LoginForm;
use mdm\admin\components\MenuHelper;
use mdm\admin\models\Menu as MenuModel;
use Yii;

/**
 * 权限管理
 *   权限列表 permissions
 *   添加权限 permission-add
 *   更新权限 permission-update
 *   删除权限 permission-delete
 *   权限详情 permission-detail
 *   权限路由 permission-route
 *
 * 菜单管理
 *   菜单列表 menus
 *   添加菜单 menu-add
 *   更新菜单 menu-update
 *   删除菜单 menu-delete
 *   查看菜单 menu-detail
 *   菜单权限 menu-permission
 *
 * @author  Wsl <wsl_ys@163.com>
 *
 * @OA\Tag(
 *     name="权限管理",
 *     description="权限管理接口文档【个人比较懒惰，权限管理处API只实现了菜单列表接口，其他的权限相关功能均使用yii2-admin自带的，如有感兴趣的可以】",
 *     *
 * )
 * Auth controller for the `v1` module
 */
class AuthController extends RestApiBaseController
{
    /**
     * Renders the index view for the module
     * @return array
     */
    public function actionIndex()
    {
        return $this->fail('未开放接口', [], '');
    }

    /**
     * 路由列表
     *
     * @OA\Get(
     *     path="/v1/auth/routes",
     *     tags={"权限管理"},
     *     summary="路由列表",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="无权访问"
     *     )
     * )
     */
    public function actionRoutes() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 权限列表
     *
     * @OA\Get(
     *     path="/v1/auth/permissions",
     *     tags={"权限管理"},
     *     summary="权限列表",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="无权访问"
     *     )
     * )
     */
    public function actionPermissions() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 用户权限列表
     *
     * @OA\Get(
     *     path="/v1/auth/permissions-user",
     *     tags={"权限管理"},
     *     summary="用户权限列表",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="无权访问"
     *     )
     * )
     */
    public function actionPermissionsUser() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 添加权限
     *
     * @OA\Post(
     *     path="/v1/auth/permission-add",
     *     tags={"权限管理"},
     *     summary="添加权限",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="desc",
     *                     type="string"
     *                 ),
     *                 example={"name": "AAA", "desc": "Jessica Smith"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionPermissionAdd() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 更新权限
     *
     * @OA\Put (
     *     path="/v1/auth/permission-update",
     *     tags={"权限管理"},
     *     summary="更新权限",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="desc",
     *                     type="string"
     *                 ),
     *                 example={"name": "AAA", "desc": "Jessica Smith"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionPermissionUpdate() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 删除权限
     *
     * @OA\Delete  (
     *     path="/v1/auth/permission-delete",
     *     tags={"权限管理"},
     *     summary="删除权限",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="权限名称",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionPermissionDelete() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 权限详情
     *
     * @OA\Get (
     *     path="/v1/auth/permission-detail",
     *     tags={"权限管理"},
     *     summary="权限详情",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="权限名称",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionPermissionDetail() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 权限路由
     *
     * @OA\Get (
     *     path="/v1/auth/permission-route",
     *     tags={"权限管理"},
     *     summary="权限路由",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="权限名称",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionPermissionRoute() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 角色列表
     *
     * @OA\Get(
     *     path="/v1/auth/roles",
     *     tags={"权限管理"},
     *     summary="角色列表 TODO",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="无权访问"
     *     )
     * )
     */
    public function actionRoles() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 角色权限
     *
     * @OA\Post(
     *     path="/v1/auth/role-add",
     *     tags={"权限管理"},
     *     summary="添加角色",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="desc",
     *                     type="string"
     *                 ),
     *                 example={"name": "AAA", "desc": "Jessica Smith"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionRoleAdd() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 更新角色
     *
     * @OA\Put (
     *     path="/v1/auth/role-update",
     *     tags={"权限管理"},
     *     summary="更新角色",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="desc",
     *                     type="string"
     *                 ),
     *                 example={"name": "AAA", "desc": "Jessica Smith"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionRoleUpdate() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 删除角色
     *
     * @OA\Delete  (
     *     path="/v1/auth/role-delete",
     *     tags={"权限管理"},
     *     summary="删除角色",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="权限名称",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionRoleDelete() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 角色详情
     *
     * @OA\Get (
     *     path="/v1/auth/role-detail",
     *     tags={"权限管理"},
     *     summary="角色详情",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="权限名称",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionRoleDetail() {
        return $this->fail('未开放接口', [], '');
    }

    /**
     * 菜单列表
     *
     * @OA\Get(
     *     path="/v1/auth/menus",
     *     tags={"权限管理"},
     *     summary="菜单列表",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="无权访问"
     *     )
     * )
     */
    public function actionMenus() {
        $callback = function($menu){
            return Func::formatMenuData($menu);
        };

        $_items = MenuHelper::getAssignedMenu(Yii::$app->user->id, null, $callback, '');
        return $this->success($_items);
    }
    /**
     * 添加菜单
     *
     * @OA\Post(
     *     path="/v1/auth/menu-add",
     *     tags={"权限管理"},
     *     summary="添加菜单",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="desc",
     *                     type="string"
     *                 ),
     *                 example={"name": "AAA", "desc": "Jessica Smith"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionMenuAdd() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 更新菜单
     *
     * @OA\Put (
     *     path="/v1/auth/menu-update",
     *     tags={"权限管理"},
     *     summary="更新菜单",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="desc",
     *                     type="string"
     *                 ),
     *                 example={"name": "AAA", "desc": "Jessica Smith"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionMenuUpdate() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 删除菜单
     *
     * @OA\Delete  (
     *     path="/v1/auth/menu-delete",
     *     tags={"权限管理"},
     *     summary="删除菜单",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="菜单名称",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionMenuDelete() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 菜单详情
     *
     * @OA\Get (
     *     path="/v1/auth/menu-detail",
     *     tags={"权限管理"},
     *     summary="菜单详情",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="菜单名称",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionMenuDetail() {
        return $this->fail('未开放接口', [], '');
    }
    /**
     * 菜单权限
     *
     * @OA\Get (
     *     path="/v1/auth/menu-permission",
     *     tags={"权限管理"},
     *     summary="菜单路由",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="菜单名称",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function actionMenuPermission() {
        return $this->fail('未开放接口', [], '');
    }

    /**
     * 退出登录
     */
    public function actionLogout() {
        return $this->success(['msg' => '用户注册接口待开发']);
    }

    public function actionSms() {
        return $this->success(['msg' => '用户注册接口待开发']);
    }

    public function action2stepCode() {
        return $this->success(['msg' => '用户注册接口待开发']);
    }

    // 用户权限
    public function actionRoute() {
        $data = [
            [
                "path" =>  "/permission",
                "redirect" =>  "/permission/page",
                "alwaysShow" =>  true,
                "name" =>  "Permission",
                "meta" =>  [
                    "title" =>  "Permission",
                    "icon" =>  "lock",
                    "roles" =>  [
                        "admin",
                        "editor"
                    ]
                ],
                "children" =>  [
                    [
                        "path" =>  "page",
                        "name" =>  "PagePermission",
                        "meta" =>  [
                            "title" =>  "Page Permission",
                            "roles" =>  [
                                "admin"
                            ]
                        ]
                    ],
                    [
                        "path" =>  "directive",
                        "name" =>  "DirectivePermission",
                        "meta" =>  [
                            "title" =>  "Directive Permission"
                        ]
                    ],
                    [
                        "path" =>  "role",
                        "name" =>  "RolePermission",
                        "meta" =>  [
                            "title" =>  "Role Permission",
                            "roles" =>  [
                                "admin"
                            ]
                        ]
                    ]
                ]
            ],
            [
                "path" =>  "/icon",
                "children" =>  [
                    [
                        "path" =>  "index",
                        "name" =>  "Icons",
                        "meta" =>  [
                            "title" =>  "Icons",
                            "icon" =>  "icon",
                            "noCache" =>  true
                        ]
                    ]
                ]
            ],
            [
                "path" =>  "/components",
                "redirect" =>  "noRedirect",
                "name" =>  "ComponentDemo",
                "meta" =>  [
                    "title" =>  "Components",
                    "icon" =>  "component"
                ],
                "children" =>  [
                    [
                        "path" =>  "tinymce",
                        "name" =>  "TinymceDemo",
                        "meta" =>  [
                            "title" =>  "Tinymce"
                        ]
                    ],
                    [
                        "path" =>  "markdown",
                        "name" =>  "MarkdownDemo",
                        "meta" =>  [
                            "title" =>  "Markdown"
                        ]
                    ],
                    [
                        "path" =>  "json-editor",
                        "name" =>  "JsonEditorDemo",
                        "meta" =>  [
                            "title" =>  "JSON Editor"
                        ]
                    ],
                    [
                        "path" =>  "split-pane",
                        "name" =>  "SplitpaneDemo",
                        "meta" =>  [
                            "title" =>  "SplitPane"
                        ]
                    ],
                    [
                        "path" =>  "avatar-upload",
                        "name" =>  "AvatarUploadDemo",
                        "meta" =>  [
                            "title" =>  "Upload"
                        ]
                    ],
                    [
                        "path" =>  "dropzone",
                        "name" =>  "DropzoneDemo",
                        "meta" =>  [
                            "title" =>  "Dropzone"
                        ]
                    ],
                    [
                        "path" =>  "sticky",
                        "name" =>  "StickyDemo",
                        "meta" =>  [
                            "title" =>  "Sticky"
                        ]
                    ],
                    [
                        "path" =>  "count-to",
                        "name" =>  "CountToDemo",
                        "meta" =>  [
                            "title" =>  "Count To"
                        ]
                    ],
                    [
                        "path" =>  "mixin",
                        "name" =>  "ComponentMixinDemo",
                        "meta" =>  [
                            "title" =>  "Component Mixin"
                        ]
                    ],
                    [
                        "path" =>  "back-to-top",
                        "name" =>  "BackToTopDemo",
                        "meta" =>  [
                            "title" =>  "Back To Top"
                        ]
                    ],
                    [
                        "path" =>  "drag-dialog",
                        "name" =>  "DragDialogDemo",
                        "meta" =>  [
                            "title" =>  "Drag Dialog"
                        ]
                    ],
                    [
                        "path" =>  "drag-select",
                        "name" =>  "DragSelectDemo",
                        "meta" =>  [
                            "title" =>  "Drag Select"
                        ]
                    ],
                    [
                        "path" =>  "dnd-list",
                        "name" =>  "DndListDemo",
                        "meta" =>  [
                            "title" =>  "Dnd List"
                        ]
                    ],
                    [
                        "path" =>  "drag-kanban",
                        "name" =>  "DragKanbanDemo",
                        "meta" =>  [
                            "title" =>  "Drag Kanban"
                        ]
                    ]
                ]
            ],
            [
                "path" =>  "/charts",
                "redirect" =>  "noRedirect",
                "name" =>  "Charts",
                "meta" =>  [
                    "title" =>  "Charts",
                    "icon" =>  "chart"
                ],
                "children" =>  [
                    [
                        "path" =>  "keyboard",
                        "name" =>  "KeyboardChart",
                        "meta" =>  [
                            "title" =>  "Keyboard Chart",
                            "noCache" =>  true
                        ]
                    ],
                    [
                        "path" =>  "line",
                        "name" =>  "LineChart",
                        "meta" =>  [
                            "title" =>  "Line Chart",
                            "noCache" =>  true
                        ]
                    ],
                    [
                        "path" =>  "mix-chart",
                        "name" =>  "MixChart",
                        "meta" =>  [
                            "title" =>  "Mix Chart",
                            "noCache" =>  true
                        ]
                    ]
                ]
            ]
        ];
        return $this->success($data, '', '登录用户信息');
    }

    /**
     * 登录用户信息
     */
    public function actionLoginUserInfo()
    {
        if (Yii::$app->user->isGuest) {
            return $this->fail('未登录', []);
        }else {
            return $this->success(Yii::$app->user->identity->toArray(), '', '登录用户信息');
        }
    }
}
