import request from '@/utils/request'

// 文章列表
export function fetchList(query) {
  return request({
    url: '/articles',
    method: 'get',
    params: query
  })
}

// 文章详情
export function fetchArticle(id) {
  return request({
    url: `/article/${id}`,
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/article/pv',
    method: 'get',
    params: { pv }
  })
}

// 创建文章
export function createArticle(data) {
  return request({
    url: '/article',
    method: 'post',
    data
  })
}

// 更新文章
export function updateArticle(data) {
  return request({
    url: `/article/${data.id}`,
    method: 'put',
    data
  })
}

// 删除文章
export function deleteArticle(id) {
  return request({
    url: '/article',
    method: 'delete',
    params: { id }
  })
}
