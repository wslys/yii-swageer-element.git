// eslint-disable-next-line
import * as userService from '@/api/user'
// eslint-disable-next-line no-unused-vars
import { constantRoutes } from '@/router/index'
import Layout from '@/layout'

// 前端路由表
export const constantRouterComponents = {
  // 基础页面 layout 必须引入
  'Layout': Layout

  // 'articleindex': () => import('@/views/article/index')
  // 'inline-edit-table': () => import('@/views/table/inline-edit-table')
  // other ...
}

// 前端未找到页面路由（固定不用改）
const notFoundRouter = {
  path: '/404',
  component: () => import('@/views/error-page/404'),
  hidden: true
}

/**
 * 动态生成菜单
 * @returns {Promise<unknown>}
 */
export const generatorRouter = () => {
  return new Promise((resolve, reject) => {
    userService.getCurrentUserNav().then(res => {
      const { data } = res
      const routes = generator(data)
      resolve(routes.concat(constantRoutes))
      // resolve(constantRoutes)
    }).catch(err => {
      reject(err)
    })
  })
}

/**
 * 格式化树形结构数据 生成 vue-router 层级路由表
 *
 * @param routerMap
 * @param parent
 * @returns {*}
 */
export const generator = (routerMap, parent) => {
  return routerMap.map(item => {
    // 动态加载路由对应的组件
    item.path = item.path || `${parent && parent.path || ''}/${item.key}`
    item.component = (constantRouterComponents[item.component || item.key]) || loadView(item.component)
    if (item.children && item.children.length > 0) {
      item.children = generator(item.children, item)
    }
    return item
  })
}

/* 加载视图 */
export const loadView = (view) => {
  return (resolve) => require([`@/views${view}`], resolve)
}

/**
 * TODO 格式化树形结构数据 生成 vue-router 层级路由表
 *
 * @param routerMap
 * @param parent
 * @returns {*}
 */
export const generator2 = (routerMap, parent) => {
  return routerMap.map(item => {
    const { title, icon, noCache, breadcrumb, affix, activeMenu, roles } = item.meta || {}
    const currentRouter = {
      // 当设置 true 的时候该路由不会在侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
      hidden: item.hidden || false,
      // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
      redirect: item.redirect || '',
      // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
      // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
      // 若你想不管路由下面的 children 声明的个数都显示你的根路由
      // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
      alwaysShow: item.redirect || false,
      // 如果路由设置了 path，则作为默认 path，否则 路由地址 动态拼接生成如 /dashboard/workplace
      path: item.path || `${parent && parent.path || ''}/${item.key}`,
      // 路由名称，建议唯一
      name: item.name || item.key.replace('/', '') || '',

      // 该路由对应页面的 组件 :方案1
      component: item.component.indexOf('@/views') > 0 ? import(item.component) : constantRouterComponents[item.component || item.key],
      // 该路由对应页面的 组件 :方案2 (动态加载)
      // component: (constantRouterComponents[item.component || item.key]) || import(`@/views/${item.component}`),
      // 下方重写动态加载组件

      // meta: 页面标题, 菜单图标, 页面权限(供指令权限用，可去掉)
      meta: {
        title: title,
        icon: icon || '',
        noCache: noCache || '',
        breadcrumb: breadcrumb || '',
        affix: affix || '',
        activeMenu: activeMenu || '',
        roles: roles || [''],
        permission: item.name // TODO
      }
    }

    // 为了防止出现后端返回结果不规范，处理有可能出现拼接出两个 反斜杠
    if (!currentRouter.path.startsWith('http')) {
      currentRouter.path = currentRouter.path.replace('//', '/')
    }

    // TODO 动态加载组件
    // if (constantRouterComponents[item.component || item.key]) {
    //   currentRouter.component = constantRouterComponents[item.component || item.key]
    // }else {
    //   // 检查上级的路由结尾处是否有结束符‘/’， 若有，则加载组件时，该路径
    //   if () {
    //   }
    // }

    // 重定向
    item.redirect && (currentRouter.redirect = item.redirect)
    // 是否有子菜单，并递归处理
    if (item.children && item.children.length > 0) {
      // Recursion
      currentRouter.children = generator(item.children, currentRouter)
    }
    return currentRouter
  })
}
