/*
 Navicat Premium Data Transfer

 Source Server         : loc
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : localhost:3306
 Source Schema         : swagger-element

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 08/11/2020 19:47:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for swel_article
-- ----------------------------
DROP TABLE IF EXISTS `swel_article`;
CREATE TABLE `swel_article`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` smallint(3) NULL DEFAULT NULL,
  `status` smallint(3) NULL DEFAULT NULL,
  `connect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `content_short` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文章摘要',
  `source_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文章外链',
  `image_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文章图片',
  `display_time` int(11) NULL DEFAULT NULL COMMENT '前台展示时间',
  `comment_disabled` smallint(3) NULL DEFAULT NULL,
  `importance` tinyint(3) NULL DEFAULT NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  `updated_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of swel_article
-- ----------------------------

-- ----------------------------
-- Table structure for swel_auth_assignment
-- ----------------------------
DROP TABLE IF EXISTS `swel_auth_assignment`;
CREATE TABLE `swel_auth_assignment`  (
  `item_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_name`, `user_id`) USING BTREE,
  INDEX `swel_idx-auth_assignment-user_id`(`user_id`) USING BTREE,
  CONSTRAINT `swel_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `swel_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of swel_auth_assignment
-- ----------------------------
INSERT INTO `swel_auth_assignment` VALUES ('创建文章', '1', 1604503739);
INSERT INTO `swel_auth_assignment` VALUES ('删除文章', '1', 1604503739);
INSERT INTO `swel_auth_assignment` VALUES ('文章列表', '1', 1604503739);
INSERT INTO `swel_auth_assignment` VALUES ('文章管理', '1', 1604503739);
INSERT INTO `swel_auth_assignment` VALUES ('文章详情', '1', 1604503739);
INSERT INTO `swel_auth_assignment` VALUES ('更新文章', '1', 1604503739);
INSERT INTO `swel_auth_assignment` VALUES ('用户Info', '1', 1604794512);
INSERT INTO `swel_auth_assignment` VALUES ('用户管理', '1', 1604794374);
INSERT INTO `swel_auth_assignment` VALUES ('菜单管理', '1', 1604815472);

-- ----------------------------
-- Table structure for swel_auth_item
-- ----------------------------
DROP TABLE IF EXISTS `swel_auth_item`;
CREATE TABLE `swel_auth_item`  (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `rule_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `data` blob NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  `updated_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE,
  INDEX `rule_name`(`rule_name`) USING BTREE,
  INDEX `swel_idx-auth_item-type`(`type`) USING BTREE,
  CONSTRAINT `swel_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `swel_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of swel_auth_item
-- ----------------------------
INSERT INTO `swel_auth_item` VALUES ('/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/assignment/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/assignment/assign', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/assignment/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/assignment/revoke', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/assignment/view', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/default/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/default/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/menu/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/menu/create', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/menu/delete', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/menu/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/menu/update', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/menu/view', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/permission/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/permission/assign', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/permission/create', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/permission/delete', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/permission/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/permission/remove', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/permission/update', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/permission/view', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/role/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/role/assign', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/role/create', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/role/delete', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/role/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/role/remove', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/role/update', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/role/view', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/route/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/route/assign', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/route/create', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/route/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/route/refresh', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/route/remove', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/rule/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/rule/create', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/rule/delete', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/rule/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/rule/update', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/rule/view', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/user/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/user/activate', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/user/change-password', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/user/delete', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/user/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/user/login', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/user/logout', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/user/request-password-reset', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/user/reset-password', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/user/signup', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/admin/user/view', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/debug/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/debug/default/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/debug/default/db-explain', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/debug/default/download-mail', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/debug/default/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/debug/default/toolbar', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/debug/default/view', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/debug/user/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/debug/user/reset-identity', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/debug/user/set-identity', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/gii/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/gii/default/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/gii/default/action', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/gii/default/diff', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/gii/default/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/gii/default/preview', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/gii/default/view', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/site/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/article/*', 2, NULL, NULL, NULL, 1604502560, 1604502560);
INSERT INTO `swel_auth_item` VALUES ('/v1/article/create', 2, NULL, NULL, NULL, 1604502560, 1604502560);
INSERT INTO `swel_auth_item` VALUES ('/v1/article/delete', 2, NULL, NULL, NULL, 1604502560, 1604502560);
INSERT INTO `swel_auth_item` VALUES ('/v1/article/index', 2, NULL, NULL, NULL, 1604502560, 1604502560);
INSERT INTO `swel_auth_item` VALUES ('/v1/article/update', 2, NULL, NULL, NULL, 1604502560, 1604502560);
INSERT INTO `swel_auth_item` VALUES ('/v1/article/view', 2, NULL, NULL, NULL, 1604502560, 1604502560);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/2step-code', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/login-user-info', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/logout', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/menu-add', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/menu-delete', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/menu-detail', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/menu-permission', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/menu-update', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/menus', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/permission-add', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/permission-delete', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/permission-detail', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/permission-route', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/permission-update', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/permissions', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/permissions-user', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/role-add', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/role-delete', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/role-detail', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/role-update', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/roles', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/route', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/routes', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/auth/sms', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/default/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/default/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/menu/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/menu/add', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/menu/delete', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/menu/list', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/menu/update', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/menu/view', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/*', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/add', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/index', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/info', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/list', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/login', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/login-user-info', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/menu', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/menus2', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/nav', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/register', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('/v1/user/route', 2, NULL, NULL, NULL, 1604501691, 1604501691);
INSERT INTO `swel_auth_item` VALUES ('创建文章', 2, NULL, NULL, NULL, 1604502596, 1604502596);
INSERT INTO `swel_auth_item` VALUES ('删除文章', 2, '删除文章内容，增加规则加以约束', 'articleDeleteRule', NULL, 1604502632, 1604792570);
INSERT INTO `swel_auth_item` VALUES ('文章列表', 2, NULL, NULL, NULL, 1604502663, 1604502663);
INSERT INTO `swel_auth_item` VALUES ('文章管理', 2, NULL, NULL, NULL, 1604502586, 1604502586);
INSERT INTO `swel_auth_item` VALUES ('文章详情', 2, '查看文章详情，增加规则加以约束', 'articleViewRule', NULL, 1604502652, 1604765056);
INSERT INTO `swel_auth_item` VALUES ('更新文章', 2, '更新文章内容，增加规则加以约束', 'articleUpdateRule', NULL, 1604502621, 1604792553);
INSERT INTO `swel_auth_item` VALUES ('用户Info', 2, NULL, NULL, NULL, 1604794341, 1604794341);
INSERT INTO `swel_auth_item` VALUES ('用户管理', 2, NULL, NULL, NULL, 1604794328, 1604794328);
INSERT INTO `swel_auth_item` VALUES ('菜单列表', 2, NULL, NULL, NULL, 1604815390, 1604815390);
INSERT INTO `swel_auth_item` VALUES ('菜单管理', 2, NULL, NULL, NULL, 1604815374, 1604815374);

-- ----------------------------
-- Table structure for swel_auth_item_child
-- ----------------------------
DROP TABLE IF EXISTS `swel_auth_item_child`;
CREATE TABLE `swel_auth_item_child`  (
  `parent` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`, `child`) USING BTREE,
  INDEX `child`(`child`) USING BTREE,
  CONSTRAINT `swel_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `swel_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `swel_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `swel_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of swel_auth_item_child
-- ----------------------------
INSERT INTO `swel_auth_item_child` VALUES ('创建文章', '/v1/article/create');
INSERT INTO `swel_auth_item_child` VALUES ('删除文章', '/v1/article/delete');
INSERT INTO `swel_auth_item_child` VALUES ('文章列表', '/v1/article/index');
INSERT INTO `swel_auth_item_child` VALUES ('更新文章', '/v1/article/update');
INSERT INTO `swel_auth_item_child` VALUES ('文章详情', '/v1/article/view');
INSERT INTO `swel_auth_item_child` VALUES ('菜单列表', '/v1/auth/menus');
INSERT INTO `swel_auth_item_child` VALUES ('用户Info', '/v1/user/info');
INSERT INTO `swel_auth_item_child` VALUES ('文章管理', '创建文章');
INSERT INTO `swel_auth_item_child` VALUES ('文章管理', '删除文章');
INSERT INTO `swel_auth_item_child` VALUES ('文章管理', '文章列表');
INSERT INTO `swel_auth_item_child` VALUES ('文章管理', '文章详情');
INSERT INTO `swel_auth_item_child` VALUES ('文章管理', '更新文章');
INSERT INTO `swel_auth_item_child` VALUES ('用户管理', '用户Info');
INSERT INTO `swel_auth_item_child` VALUES ('菜单管理', '菜单列表');

-- ----------------------------
-- Table structure for swel_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `swel_auth_rule`;
CREATE TABLE `swel_auth_rule`  (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data` blob NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  `updated_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of swel_auth_rule
-- ----------------------------
INSERT INTO `swel_auth_rule` VALUES ('articleDeleteRule', 0x4F3A34363A226170695C6D6F64756C65735C76315C72756C65735C61727469636C655C41727469636C6544656C65746552756C65223A333A7B733A343A226E616D65223B733A31373A2261727469636C6544656C65746552756C65223B733A393A22637265617465644174223B693A313630343739323431353B733A393A22757064617465644174223B693A313630343739323431353B7D, 1604792415, 1604792415);
INSERT INTO `swel_auth_rule` VALUES ('articleUpdateRule', 0x4F3A34363A226170695C6D6F64756C65735C76315C72756C65735C61727469636C655C41727469636C6555706461746552756C65223A333A7B733A343A226E616D65223B733A31373A2261727469636C6555706461746552756C65223B733A393A22637265617465644174223B693A313630343739323435303B733A393A22757064617465644174223B693A313630343739323435303B7D, 1604792450, 1604792450);
INSERT INTO `swel_auth_rule` VALUES ('articleViewRule', 0x4F3A34343A226170695C6D6F64756C65735C76315C72756C65735C61727469636C655C41727469636C655669657752756C65223A333A7B733A343A226E616D65223B733A31353A2261727469636C655669657752756C65223B733A393A22637265617465644174223B693A313630343736353031373B733A393A22757064617465644174223B693A313630343736353230363B7D, 1604765017, 1604765206);

-- ----------------------------
-- Table structure for swel_menu
-- ----------------------------
DROP TABLE IF EXISTS `swel_menu`;
CREATE TABLE `swel_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parent` int(11) NULL DEFAULT NULL,
  `route` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order` int(11) NULL DEFAULT NULL,
  `data` blob NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent`(`parent`) USING BTREE,
  CONSTRAINT `swel_menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `swel_menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of swel_menu
-- ----------------------------
INSERT INTO `swel_menu` VALUES (1, '文章管理', NULL, '/v1/article/index', 10100, 0x7B2270617468223A225C2F61727469636C65222C22636F6D706F6E656E74223A224C61796F7574222C227265646972656374223A226E6F5265646972656374222C226E616D65223A2241727469636C6573222C226D657461223A7B227469746C65223A225C75363538375C75376165305C75376261315C7537343036222C2269636F6E223A22636F6D706F6E656E74227D7D);
INSERT INTO `swel_menu` VALUES (2, '文章列表', 1, '/v1/article/index', 10101, 0x7B2270617468223A225C2F61727469636C655C2F696E646578222C22636F6D706F6E656E74223A225C2F61727469636C655C2F696E646578222C226E616D65223A2241727469636C6573222C226D657461223A7B227469746C65223A225C75363538375C75376165305C75353231375C7538383638222C2269636F6E223A226C6F636B227D7D);
INSERT INTO `swel_menu` VALUES (3, '创建文章', 1, '/v1/article/create', 10102, 0x7B2270617468223A225C2F61727469636C655C2F637265617465222C22636F6D706F6E656E74223A225C2F61727469636C655C2F637265617465222C226E616D65223A2241727469636C65437265617465222C226D657461223A7B227469746C65223A225C75353231625C75356566615C75363538375C7537616530222C2269636F6E223A226C6F636B222C22726F6C6573223A5B2261646D696E222C22656469746F72225D7D7D);
INSERT INTO `swel_menu` VALUES (4, '文章详情', 1, '/v1/article/view', 10103, 0x7B2270617468223A225C2F61727469636C655C2F76696577222C22636F6D706F6E656E74223A225C2F61727469636C655C2F76696577222C226E616D65223A2241727469636C6556696577222C226D657461223A7B227469746C65223A225C75363538375C75376165305C75386265365C7536306335222C2269636F6E223A226C6F636B222C22726F6C6573223A5B2261646D696E222C22656469746F72225D7D7D);
INSERT INTO `swel_menu` VALUES (5, '更新文章', 1, '/v1/article/update', 10104, 0x7B2270617468223A225C2F61727469636C655C2F757064617465222C22636F6D706F6E656E74223A225C2F61727469636C655C2F757064617465222C226E616D65223A2241727469636C65557064617465222C226D657461223A7B227469746C65223A225C75363666345C75363562305C75363538375C7537616530222C2269636F6E223A226C6F636B222C22726F6C6573223A5B2261646D696E222C22656469746F72225D7D7D);

-- ----------------------------
-- Table structure for swel_migration
-- ----------------------------
DROP TABLE IF EXISTS `swel_migration`;
CREATE TABLE `swel_migration`  (
  `version` varchar(180) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `apply_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of swel_migration
-- ----------------------------
INSERT INTO `swel_migration` VALUES ('m000000_000000_base', 1604494346);
INSERT INTO `swel_migration` VALUES ('m130524_201442_init', 1604494349);
INSERT INTO `swel_migration` VALUES ('m140506_102106_rbac_init', 1604494558);
INSERT INTO `swel_migration` VALUES ('m140602_111327_create_menu_table', 1604494526);
INSERT INTO `swel_migration` VALUES ('m160312_050000_create_user', 1604494527);
INSERT INTO `swel_migration` VALUES ('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1604494558);
INSERT INTO `swel_migration` VALUES ('m180523_151638_rbac_updates_indexes_without_prefix', 1604494558);
INSERT INTO `swel_migration` VALUES ('m190124_110200_add_verification_token_column_to_user_table', 1604494349);
INSERT INTO `swel_migration` VALUES ('m200409_110543_rbac_update_mssql_trigger', 1604494558);

-- ----------------------------
-- Table structure for swel_user
-- ----------------------------
DROP TABLE IF EXISTS `swel_user`;
CREATE TABLE `swel_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `access_token` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `expire` int(11) NULL DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  UNIQUE INDEX `password_reset_token`(`password_reset_token`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of swel_user
-- ----------------------------
INSERT INTO `swel_user` VALUES (1, 'admin', '4ZwES0RzUXBnc6Lao7DR2Sad01SltfnD', '$2y$13$fJPQDgPA8/gn4afjmWnR0uNTz7YevygtLFtnXK2/0cWWsJFnZz.gi', NULL, NULL, 'wsl_ys@163.com', '5nSLq3hpW74l9YvszI9bkIydOJHIvdDO', 1604833030, 10, 1604499323, 1604829430, 'JRt-S9PLydU_zwhXsxXfW0mjup0N5gxl_1604499323');
INSERT INTO `swel_user` VALUES (2, 'wsl', 'SW9ejQZ_CB7b8Ul1xB4edO-7DbrXmjmG', '$2y$13$vkWGpcJONd3dr7BeUu/Qg.fBZPpZwJksB/OEFYvSOkyyJ.Saq.tIq', NULL, NULL, 'wsl@163.com', NULL, NULL, 10, 1604499716, 1604499716, 'il4aUeb8Yb0DXnh2AKDJKwHFUmuEOFCZ_1604499716');

SET FOREIGN_KEY_CHECKS = 1;
